using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Question : MonoBehaviour
{
    public GameObject _help;
    public Button _helpButton1, _helpButton2, _helpButton3;
    public Text _countdownText;
    public Image _countdownImage;
    float _countTime = 30;
    bool _bonusTime;
    bool _startTime;

    int _talkNPC = 1;
    public GameObject _talk;
    public Text _descriptionText;
    bool _cooldown;

    public Text _questNumText;
    public Text _questionText;
    int _questNum = 1;
    int _randomNum;

    public Text _ansAText , _ansBText , _ansCText , _ansDText;
    public Button _ansAButton , _ansBButton , _ansCButton , _ansDButton;
    int _collect;

    List<int> list = new List<int>();
    int _randomRemove;

    private void Start()
    {
        GenerateRandom(1, 11);
    }
    void Update()
    {
        _questNumText.text = "�Ӷ����� : " + _questNum;
        NpcTalk();
    }
    private void FixedUpdate()
    {
        StartTime();
    }
    void StartTime()
    {
        if (_startTime == true)
        {
            _countdownText.text = Mathf.Round(_countTime).ToString();
            _countdownImage.fillAmount -= Time.deltaTime;

            if (_countdownImage.fillAmount <= 0)
            {
                _countTime -= 1;
                _countdownImage.fillAmount = 1;
            }

            if (_countTime <= 0)
            {
                _countTime = 0;
                _countdownText.text = "" + _countTime;
                _countdownImage.fillAmount = 0;
            }
        }

        if (_countTime >= 30 && _bonusTime == false)
        {
            _countTime = 30;
        }
        if (_countTime <= 0)
        {
            _talkNPC = 12;
            _countTime += 30;
        }
    }
    void NpcTalk()
    {
        if (_cooldown == false)
        {
            _talk.SetActive(true);
            switch (_talkNPC)
            {
                case 1:
                    _startTime = false;
                    _descriptionText.text = "\"" + "���ʴ�" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 2:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�ѹ�繾Ըա� ���Թ��¡��" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 3:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�͵�ͧ�ͺ �Ӷ�����١ ��ͧ������ 10 ���" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 4:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�������������ѹ���" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 5:
                    _help.SetActive(true);
                    _startTime = true;
                    _talk.SetActive(false);
                    numQuest();
                    break;
                case 6:
                    _startTime = false;
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 7:
                    _startTime = true;
                    _talk.SetActive(false);
                    numQuest();
                    break;
                case 8:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�ͺ�١��ͧ" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 9:
                    _startTime = true;
                    _talk.SetActive(false);
                    numQuest();
                    break;
                case 10:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�ͺ�ѧ���١��ͧ��" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 11:
                    SceneManager.LoadScene(1);
                    break;
                case 12:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�����������! �������蹡ѹ�����" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 13:
                    SceneManager.LoadScene(1);
                    break;
                case 14:
                    _startTime = false;
                    _descriptionText.text = "\"" + "�觨ѧ��� ! �ͺ�١����ء���" + "\"";
                    StartCoroutine(cooldownTalk(3));
                    _cooldown = true;
                    break;
                case 15:
                    SceneManager.LoadScene(1);
                    break;
            }
        }
    }
    void numQuest()
    {
        switch (_randomNum)
        {
            case 1 :
                _questionText.text = "�ѵ�쪹Դ����ѵ�캡";
                _ansAText.text = "��� 1: ���";
                _ansBText.text = "��� 2: ��";
                _ansCText.text = "��� 3: ���";
                _ansDText.text = "��� 4: ���";
                _collect = 4;
                break;
            case 2:
                _questionText.text = "���ա���";
                _ansAText.text = "��� 1: �ͧ��";
                _ansBText.text = "��� 2: ����";
                _ansCText.text = "��� 3: ˡ��";
                _ansDText.text = "��� 4: Ỵ��";
                _collect = 1;
                break;
            case 3:
                _questionText.text = "����ص������͹����ش";
                _ansAText.text = "��� 1: �������";
                _ansBText.text = "��� 2: ���������";
                _ansCText.text = "��� 3: �����";
                _ansDText.text = "��� 4: �ū�";
                _collect = 3;
                break;
            case 4:
                _questionText.text = "���繾���͡�����ͧ DragonBall";
                _ansAText.text = "��� 1: ⧡ع";
                _ansBText.text = "��� 2: �ը��";
                _ansCText.text = "��� 3: �����Թ";
                _ansDText.text = "��� 4: �Ԥ����";
                _collect = 1;
                break;
            case 5:
                _questionText.text = "�Էҹ����ͧ��е��¡Ѻ��� ���繼�骹�";
                _ansAText.text = "��� 1: ��е���";
                _ansBText.text = "��� 2: ���";
                _ansCText.text = "��� 3: ����͡";
                _ansDText.text = "��� 4: ˹�";
                _collect = 2;
                break;
            case 6:
                _questionText.text = "��ҹ����� KFC �������";
                _ansAText.text = "��� 1: ���";
                _ansBText.text = "��� 2: ���";
                _ansCText.text = "��� 3: ��";
                _ansDText.text = "��� 4: ��";
                _collect = 4;
                break;
            case 7:
                _questionText.text = "����������ͻ���पѹ��������";
                _ansAText.text = "��� 1: LineMan";
                _ansBText.text = "��� 2: Gojek";
                _ansCText.text = "��� 3: Facebook";
                _ansDText.text = "��� 4: GrabFood";
                _collect = 3;
                break;
            case 8:
                _questionText.text = "���������� Battle Royale";
                _ansAText.text = "��� 1: Getamped";
                _ansBText.text = "��� 2: Pubg";
                _ansCText.text = "��� 3: Yulgang";
                _ansDText.text = "��� 4: Ragnarok";
                _collect = 2;
                break;
            case 9:
                _questionText.text = "���� Moba �·������蹽���С�褹";
                _ansAText.text = "��� 1: 3-3";
                _ansBText.text = "��� 2: 5-5";
                _ansCText.text = "��� 3: 7-7";
                _ansDText.text = "��� 4: 10-10";
                _collect = 2;
                break;
            case 10:
                _questionText.text = "����ҹ� ZaiStudio �����ҧ��";
                _ansAText.text = "��� 1: ˹�ҵҴ�";
                _ansBText.text = "��� 2: �繤���";
                _ansCText.text = "��� 3: ��ѹ�ѹ��";
                _ansDText.text = "��� 4: �١�ء���";
                _collect = 4;
                break;
        }
    }
    public void ansCollectButton(int collect)
    {
        if (_collect == collect)
        {
            ButtonRemove(_ansAButton, _randomRemove, 1, true);
            ButtonRemove(_ansBButton, _randomRemove, 2, true);
            ButtonRemove(_ansCButton, _randomRemove, 3, true);
            ButtonRemove(_ansDButton, _randomRemove, 4, true);
            _bonusTime = false;
            _countTime += 30;
            _questNum += 1;
            _talkNPC = 8;
            GenerateRandom(1, 11);
        }
        else
        {
            _talkNPC = 10;
        }
    }
    void GenerateRandom(int min, int max)
    {
        if (_questNum <= 10)
        {
            _randomNum = Random.Range(min, max);
            while (list.Contains(_randomNum))
            {
                _randomNum = Random.Range(min, max);
            }
            list.Add(_randomNum);
        }
        else
        {
            _talkNPC = 14;
        }
    }
 
    public void Help1()
    {
        _bonusTime = true;
        _countTime += _countTime;
        _helpButton1.gameObject.SetActive(false);
    }

    public void Help2()
    {
        List<int> list = new List<int>(new int[5]);

        for (int i = 0; i < 2; i++)
        {
            _randomRemove = Random.Range(1, 5);

            do
            {
                while (list.Contains(_randomRemove))
                {
                    _randomRemove = Random.Range(1, 5);
                }
                list[i] = _randomRemove;
            }

            while (_randomRemove == _collect);
            ButtonRemove(_ansAButton, _randomRemove, 1, false);
            ButtonRemove(_ansBButton, _randomRemove, 2, false);
            ButtonRemove(_ansCButton, _randomRemove, 3, false);
            ButtonRemove(_ansDButton, _randomRemove, 4, false);
        }
        _helpButton2.gameObject.SetActive(false);

    }
    void ButtonRemove(Button button , int random , int num , bool checkObj)
    {
        if (random == num && checkObj == false)
        {
            button.gameObject.SetActive(false);
        }
        else if (checkObj == true)
        {
            button.gameObject.SetActive(true);
        }
 
    }
    public void Help3()
    {
        _talkNPC = 6;
        float random = Random.value;

        if (random > 0.6) //%40 percent chance
        {
            _descriptionText.text = "\"" + "���������Թ�" + "\"";
        }

        else if (random < 0.4) //%40 percent chance
        {
            _descriptionText.text = "\"" + "�ӵͺ���١��ͧ��͢�� : " + _collect + "\"";
        }
        else //%20 percent chance
        {
            int randomChance = Random.Range(1, 5);
            while (randomChance == _collect)
            {
                randomChance = Random.Range(1, 5);
                print(randomChance);
            }
            _descriptionText.text = "\"" + "�ӵͺ���١��ͧ��͢�� : " + randomChance + "\"";
        }
        _helpButton3.gameObject.SetActive(false);
    }
    IEnumerator cooldownTalk(float time)
    {
        yield return new WaitForSeconds(time);
        _talkNPC += 1;
        _cooldown = false;
    }
}
