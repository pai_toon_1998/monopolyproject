using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeSetting : MonoBehaviour
{

    public List<InputField> _hoursFieldList;
    public List<InputField> _minuteFieldList;

    public static int _hoursNum;
    public static int _minuteNum;
    public static int _totalMinuteTime;

    string _hours;
    string _minute;

    bool _isFocusHours;
    bool _isFocusMinute;
    void Awake()
    {
        ChangedHours(_hoursFieldList);
        ChangedMinute(_minuteFieldList);
    }
    private void Start()
    {
        HoursTextForm(_hoursFieldList);
        MinuteTextForm(_minuteFieldList);
    }
    void Update()
    {

        Debug.Log(_hoursNum + " , " + _minuteNum);
        ProcessMinuteTime();
        LimitMinuteTime();
        OnSelectedHoursInputField(_hoursFieldList);
        OnSelectedMinuteInputField(_minuteFieldList);
    }
    void ProcessMinuteTime()
    {
        int _hoursProcess = _hoursNum * 60;
        int _minuteProcess = _minuteNum;
        _totalMinuteTime = _hoursProcess + _minuteProcess;
    }
    void LimitMinuteTime()
    {
        if (_hoursNum >= 24)
        {
            _hoursNum = 23;
        }
        if (_minuteNum >= 60)
        {
            _minuteNum = 59;
        }
    }
    void ChangedHours(List<InputField> list)
    {
        for (int num = 0; num < 24; num++)
        {
            list[num].onValueChanged.AddListener(delegate { ValueChangeHours(_hoursFieldList); });
        }
    }
    void ChangedMinute(List<InputField> list)
    {
        for (int num = 0; num < 60; num++)
        {
            list[num].onValueChanged.AddListener(delegate { ValueChangeMinute(_minuteFieldList); });
        }
    }
    void ValueChangeHours(List<InputField> list)
    {
        for (int num = 0; num < 24; num++)
        {
            if (list[num].selectionFocusPosition == 1)
            {
                list[num].selectionAnchorPosition = 2;
                list[num].selectionFocusPosition = 1;
            }
            else
            {
                list[num].selectionAnchorPosition = 1;
                list[num].selectionFocusPosition = 0;
            }
        }
    }
    void ValueChangeMinute(List<InputField> list)
    {
        for (int num = 0; num < 60; num++)
        {
            if (list[num].selectionFocusPosition == 1)
            {
                list[num].selectionAnchorPosition = 2;
                list[num].selectionFocusPosition = 1;
            }
            else
            {
                list[num].selectionAnchorPosition = 1;
                list[num].selectionFocusPosition = 0;
            }
        }
    }
    void HoursTextForm(List<InputField> list)
    {
        _isFocusHours = false;

        for (int num = 0; num < 24; num++)
        {
            list[num].text = string.Format("{0:00}", _hoursNum);
        }

    }
    void MinuteTextForm(List<InputField> list)
    {
        _isFocusMinute = false;

        for (int num = 0; num < 60; num++)
        {
            list[num].text = string.Format("{0:00}", _minuteNum);
        }

    }
    void ReadHoursTimeOutput(List<InputField> list, int min, int max)
    {
        if (min >= max)
        {
            for (int num = 0; num < 24; num++)
            {
                list[num].text = string.Format("{0:00}", max);
            }
        }
    }
    void ReadMinuteTimeOutput(List<InputField> list, int min, int max)
    {
        if (min >= max)
        {
            for (int num = 0; num < 60; num++)
            {
                list[num].text = string.Format("{0:00}", max);
            }
        }

    }
    public void OnValueChangedHours(string h)
    {
        _hours = h;
        if (_hours != "")
        {
            _hoursNum = int.Parse(_hours);
        }
        ReadHoursTimeOutput(_hoursFieldList, _hoursNum, 23);
    }
    public void OnValueChangedMinute(string m)
    {
        _minute = m;

        if (_minute != "")
        {
            _minuteNum = int.Parse(_minute);
        }
        ReadMinuteTimeOutput(_minuteFieldList, _minuteNum, 59);
    }
    void OnSelectedHoursInputField(List<InputField> list)
    {
        for (int num = 0; num < 24; num++)
        {
            if (list[num].isFocused && _isFocusHours == false)
            {
                list[num].text = "";
                _isFocusHours = true;
            }
        }
    }
    void OnSelectedMinuteInputField(List<InputField> list)
    {
        for (int num = 0; num < 60; num++)
        {
            if (list[num].isFocused && _isFocusMinute == false)
            {
                list[num].text = "";
                _isFocusMinute = true;
            }
        }
    }
    void ScrollSnapHoursPage(List<InputField> list)
    {
        for (int num = 0; num < 24; num++)
        {
            list[num].text = string.Format("{0:00}", num);
        }

    }
    void ScrollSnapMinutePage(List<InputField> list)
    {
        for (int num = 0; num < 60; num++)
        {
            list[num].text = string.Format("{0:00}", num);
        }
    }
    public void OnEditHours() => HoursTextForm(_hoursFieldList);
    public void OnEditMinute() => MinuteTextForm(_minuteFieldList);
    public void PressRollHoursPanel() => ScrollSnapHoursPage(_hoursFieldList);
    public void PressRollMinutePanel() => ScrollSnapMinutePage(_minuteFieldList);

}